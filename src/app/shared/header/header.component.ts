import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // MENU
   var menuButton = $('.menu-button');
   var menuBar = $('.menu-bar');
   var body = $('body');

   menuButton.click(function(){
       menuBar.toggleClass('menuBarActive');
       body.toggleClass('bodyBlack');
   })
   // FIM MENU
  }

}
