import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  slides = [
    {img: "../../assets/BLACK-FRIDAY.png"},
    {img: "../../assets/BLACK-FRIDAY.png"},
    {img: "../../assets/BLACK-FRIDAY.png"},
    {img: "../../assets/BLACK-FRIDAY.png"}
  ];
  slideConfig = {"slidesToShow": 1, "slidesToScroll": 1};
  
  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }

  ngOnInit() {
  }
}
